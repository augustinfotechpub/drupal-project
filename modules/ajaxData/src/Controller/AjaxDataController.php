<?php
namespace Drupal\ajaxData\Controller;
 
use Drupal\Core\Controller\ControllerBase;
 
class AjaxDataController extends ControllerBase {

  public function load_ajax_data(){
     
    $Actionmethod = \Drupal::request()->request->get("action"); 
    $limit = \Drupal::request()->request->get("limit"); 
    $no_of_page = \Drupal::request()->request->get("no_of_page"); 
    $cat_filter = \Drupal::request()->request->get("cat_filter"); 
    $search_keyword = \Drupal::request()->request->get("search_keyword"); 
    $date_filter = \Drupal::request()->request->get("date_filter"); 
    
    $method = htmlspecialchars($Actionmethod);
    
    $path= empty($type) ? \Drupal::theme()->getActiveTheme()->getPath() :  drupal_get_path('module', $type);

    $url_file = $path."/load_ajax_data.php";
    require $url_file;

    if(($method == 'view_more_faculty') || ($method == 'view_more_career')){
      $method($limit, $no_of_page);
    }

    if($method == 'view_more_news'){
      $method($limit, $no_of_page, $date_filter);
    }

    if($method == 'view_more_events'){
      $method($limit, $no_of_page, $cat_filter);
    }

    if($method == 'view_more_resources'){
      $method($limit, $no_of_page, $cat_filter, $search_keyword);
    }

    exit();
  }
 
}

