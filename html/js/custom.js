jQuery( document ).ready(function($) {

  if (window.matchMedia("(min-width: 992px)").matches) {
    var sections = $('.section');
    var nav = $('nav.navbar');
    var nav_height = nav.innerHeight();
  
    $(window).on('scroll', function () {
      var cur_pos = $(this).scrollTop();
      
      sections.each(function() {
        var top = $(this).offset().top - nav_height,
        bottom = top + $(this).outerHeight();
        
        if (cur_pos >= top && cur_pos <= bottom) {
          nav.find('a[data-scroll]').removeClass('active');
          sections.removeClass('active');
          
          $(this).addClass('active');
          nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
        }
      });
    });

    nav.find('a[data-scroll]').on('click', function () {
      var $el = $(this)
        , id = $el.attr('href');
      // $('html, body').animate({
      //   // scrollTop: $(id).offset().top - nav_height
      //   scrollTop: $($(this).attr('href')).offset().top,
      // }, 1000, 'linear');
      scrollToAnimate(id);
      
      return false;
    });
  }

  if (window.matchMedia("(max-width: 991px)").matches) {
    var sections = $('.section');
    var nav = $('.ste-header .navbar-toggler');
    var nav_height = nav.innerHeight();
  
    $(window).on('scroll', function () {
      var cur_pos = $(this).scrollTop();
      
      sections.each(function() {
        var top = $(this).offset().top - nav_height,
        bottom = top + $(this).outerHeight();
        
        if (cur_pos >= top && cur_pos <= bottom) {
          nav.find('a[data-scroll]').removeClass('active');
          sections.removeClass('active');
          
          $(this).addClass('active');
          nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
        }
      });
    });

    nav.find('a[data-scroll]').on('click', function (e) {
      var $el = $(this)
        , id = $el.attr('href');
      // $('html, body').animate({
      //   // scrollTop: $(id).offset().top - nav_height
      //   scrollTop: $($(this).attr('href')).offset().top,
      // }, 1000, 'linear');
      nav.find('a[data-scroll]').removeClass('active');
      console.log('hi');
      $(this).addClass('active');
      scrollToAnimate(id);
      return false;
    });
  }
  
  

  if (window.matchMedia("(max-width: 991px)").matches) {
    $('.navbar a[data-scroll]').on('click', function(){
        $('#navbarNavMobile').collapse('hide');
        $("body").removeClass("navbar-show");
    });
  }

  $("html:not([dir='rtl']) .achievements-slider").owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    dots: true,
    navText: [
      '<i class="fal fa-chevron-left"></i>',
      '<i class="fal fa-chevron-right"></i>'
    ],
    responsive:{
        0:{
            items: 1,
            nav: false,
        },
        768:{
            items: 2
        },
        1000:{
            items: 3
        }
    }
  });

  $("html[dir='rtl'] .achievements-slider").owlCarousel({
    rtl:true,
    loop: true,
    margin: 0,
    nav: true,
    dots: true,
    navText: [
      '<i class="fal fa-chevron-left"></i>',
      '<i class="fal fa-chevron-right"></i>'
    ],
    responsive:{
        0:{
            items: 1,
            nav: false,
        },
        768:{
            items: 2
        },
        1000:{
            items: 3
        }
    }
  });

  if (window.matchMedia("(min-width: 992px)").matches) {
    if($(".sec-sitemap-left-inner").length) {
      
      $(".sec-sitemap-left-inner").mCustomScrollbar({
        mouseWheel:{
          enable:true,
          scrollAmount:"auto",
          axis:"y",
          preventDefault:true,
        },
        contentTouchScroll:25,
        setWidth:false,
        setHeight:false,
        setTop:0,
        setLeft:0,
        axis:"y",
      });

    }
  }

  if (window.matchMedia("(min-width: 768px)").matches) {

    $("html:not([dir='rtl']) .delivery-process-slider").owlCarousel({
      nav: false,
      dots: false,
      autoWidth:true,
      items: 3,
      navText: [
        '<i class="fal fa-chevron-left"></i>',
        '<i class="fal fa-chevron-right"></i>'
      ],
      responsive:{
          0:{
              items: 1,
              nav: false,
          },
          768:{
            margin: 50,
          },
          1200:{
            margin: 135,
          }
      }
    });

    $("html[dir='rtl'] .delivery-process-slider").owlCarousel({
      rtl:true,
      nav: false,
      dots: false,
      autoWidth:true,
      items: 3,
      navText: [
        '<i class="fal fa-chevron-left"></i>',
        '<i class="fal fa-chevron-right"></i>'
      ],
      responsive:{
          0:{
              items: 1,
              nav: false,
          },
          768:{
            margin: 50,
          },
          1200:{
            margin: 135,
          }
      }
    });
    

  }

  if($('#resources').length) {
    var $container = $('#resources').isotope({
      itemSelector : '.resources-item',
      isFitWidth: true,
      layoutMode: 'fitRows',
      resizable: false, // disable normal resizing
    });
    
    
    $container.isotope({ filter: '*' });

    if (window.matchMedia("(min-width: 768px)").matches) {
      // filter items on button click
      $('#resourcesFiltersDesktop').on( 'click', 'button', function() {
        $('#resourcesFiltersDesktop button').removeClass("active");
        $(this).addClass("active"); 
        var filterValue = $(this).attr('data-filter');
        $container.isotope({ filter: filterValue });

      });
    }

    if (window.matchMedia("(max-width: 767px)").matches) {
      var getValue = $('.resources-dropdown-menu li[selected]').html();
      $('.dropdown-select').html(getValue);

      $('.resources-dropdown-menu li').on('click', function() {
        
        $('.resources-dropdown-menu li').removeClass("active");
        $(this).addClass("active"); 
        // var getValue = $(this).text();
        var getValue = $(this).html();
        // $('.dropdown-select').text(getValue);
        $('.dropdown-select').html(getValue);
        var filterValue = $(this).attr('data-filter');
        $container.isotope({ filter: filterValue });
      });
    }
    
      
  }
  
    
});

/* [Scroll to section by Hash/Id] */
window.scrollToAnimate = function (hash) {
  // console.log(hash);
  if (hash != null && hash != undefined) {
    var scrollPos = $(hash).offset().top;
    var offset = $(".navbar.fixed-top").outerHeight();
    // $(window).scrollTop(scrollPos - offset);
    $('html, body').animate({
      scrollTop: scrollPos - offset
    }, 600, function(){
      // Add hash (#) to URL when done scrolling (default click behavior)
      // window.location.hash = hash;
    });
  }
}