jQuery( document ).ready(function($) {
  
  $(".owl-carousel.top-banner-slider").owlCarousel({
    items:1,
    autoplay:true,
    autoplayTimeout:4000,
    loop:true,
    nav:false,
  });
  $("html:not([dir='rtl']) .owl-carousel.testimonials-slider").owlCarousel({
      items:1,
      margin:20,
      nav:true,
      responsive:{
        0:{
            dots: true,
        },
        768:{
          dots: false,
        },
      }
  });
  $("html[dir='rtl'] .owl-carousel.testimonials-slider").owlCarousel({
      rtl:true,
      items:1,
      margin:20,
      nav:true,
      responsive:{
        0:{
            dots: true,
        },
        768:{
          dots: false,
        },
     }
  });
  $(window).scroll(function() {
      if ($(this).scrollTop() > 250){  
          $('header .navbar.navbar-expand-lg').addClass("sticky");
      }
      else{
          $('header .navbar.navbar-expand-lg').removeClass("sticky");
      }
  });
  $('#video-btn').click(function () {
    var video = $("#videoview").get(0);
    if ( video.paused ) {
        video.play();
        $(".play").hide();
        $(".pause").show();
    } else {
        video.pause();
        $(".play").show();
        $(".pause").hide();
    }
    return false;
 });

  $(".navbar-toggler").click(function(){
    $("body").toggleClass("navbar-show");
  });

  $("a[href='#top']").click(function() {
    $("html, body").scrollTop(0);
    return false;
  });

});

function handleSelect(elm) {
  window.location = elm.value;
}