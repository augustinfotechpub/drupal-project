<?php
/**
 * @file
 * theme-settings.php
 *
 * Provides theme settings for HEC theme.
 */

use Drupal\Core\Form\FormStateInterface;

function hec_form_system_theme_settings_alter(&$form, \Drupal\Core\Form\FormStateInterface &$form_state, $form_id = null) {
    $pages_list = getPagesList();

    $form['featured_program'] = array(
        '#type'          => 'fieldset',
        '#title'         => t('Custom program for homepage.'),
    );
    
    $form['featured_program']['hec_featured_program'] = array(
        '#type'          => 'select',  
        '#title'		=>	t("Select custom program"), 
        '#default_value' => theme_get_setting('hec_featured_program'),
        '#multiple' => TRUE,
        '#size'	=> 10,
        '#options'=> $pages_list
    );

    $form['secondary_logo_settings'] = array(
        '#type'          => 'fieldset',
        '#title'         => t('Secondary Logos'),
    );

    $form['secondary_logo_settings']['light_logo'] = [
        '#type' => 'textfield',
        '#title' => t('Path for Light Logo'),
        '#default_value' => theme_get_setting('light_logo'),
    ];

    $form['secondary_logo_settings']['dark_logo'] = [
        '#type' => 'textfield',
        '#title' => t('Path to Dark Logo'),
        '#default_value' => theme_get_setting('dark_logo'),
    ];

    $form['social_media_settings'] = array(
        '#type'          => 'fieldset',
        '#title'         => t('Social media links'),
    );
    
    $form['social_media_settings']['twitter_social_media_icon'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Twitter'),
        '#default_value' => theme_get_setting('twitter_social_media_icon'),
        '#description'   => t("Social Media.(Enter Twitter Link)"),
    );

    $form['social_media_settings']['youtube_social_media_icon'] = array(
        '#type'          => 'textfield',
        '#title'         => t('YouTube'),
        '#default_value' => theme_get_setting('youtube_social_media_icon'),
        '#description'   => t("Social Media.(Enter YouTube Link)"),
    );

    $form['social_media_settings']['facebook_social_media_icon'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Facebook'),
        '#default_value' => theme_get_setting('facebook_social_media_icon'),
        '#description'   => t("Social Media.(Enter Facebook Link)"),
    );

    $form['social_media_settings']['insta_social_media_icon'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Instagram'),
        '#default_value' => theme_get_setting('insta_social_media_icon'),
        '#description'   => t("Social Media.(Enter Instagram Link)"),
    );

    $form['social_media_settings']['linkedin_social_media_icon'] = array(
        '#type'          => 'textfield',
        '#title'         => t('Linked in'),
        '#default_value' => theme_get_setting('linkedin_social_media_icon'),
        '#description'   => t("Social Media.(Enter Linked in Link)"),
    ); 
    
    $form['copyright_text'] = array(
        '#type'          => 'textarea',
        '#title'         => t('Copyright Text'),
        '#default_value' => theme_get_setting('copyright_text'),
        '#description'   => t("Copyright Text.(Enter Copyright Text)"),
    );
}