<?php
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\taxonomy\Entity\Term;

use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\Core\Url;

use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\block\Entity\Block;
use Drupal\Core\Database\Database;

function view_more_resources($limit, $no_of_page, $cat_filter, $search_keyword){
    $start_pos = 0;
    $start_pos = ($no_of_page - 1) * $limit;

    $query = \Drupal::entityQuery('node');
	$query->condition('type', 'resources');
    $query->condition('status', 1);
    $query->sort('created', 'DESC');
    
    if($cat_filter){
        $query->condition('field_resources_category', $cat_filter);
    }

    if($search_keyword){
        $orConditions = $query->orConditionGroup();
        
        $orConditions->condition('title', '%' . $search_keyword . '%', 'LIKE');

        $query->condition($orConditions);
    }

    $query->range($start_pos, $limit); 
	$nids = $query->execute();

    $nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

    if( $nodes ) {
		foreach( $nodes as $node ) {
            $node_title = $node->get('title')->getValue();
            $resources_title = $node_title[0]['value'];
            
            $node_body = $node->get('body')->getValue();

            if (isset($node_body[0]['summary'])) {
				$body_Data = strip_tags($node_body[0]['summary']);
			} else {
				$body_Data = '';
			}

            if ($body_Data != '') {
				$body_full = $body_Data;
			} else {
				$body_full = '';
            }
            
            $resources_url = $node->toUrl()->setAbsolute()->toString();

            $node_image = $node->get('field_image')->getValue();
			$resources_image = [];
            
			if (isset($node_image[0])) {
				$file = File::load($node_image[0]['target_id']);
				$resources_image_url = $file->url();
			} else {
				$resources_image_url = '';
            }
            
            $node_resources_type = $node->get('field_resources_type')->getValue();

            if (isset($node_resources_type[0]['value'])) {
				$resources_type = $node_resources_type[0]['value'];
			} else {
				$resources_type = '';
			}

			$node_bg_color = $node->get('field_block_background_color')->getValue();

            if (isset($node_bg_color[0]['color'])) {
				$resources_bg_color = $node_bg_color[0]['color'];
			} else {
				$resources_bg_color = '#003671';
			}

            if( $resources_type == 'video' ):
                echo '<div class="col-md-6 col-lg-6 col-xl-4 resources-item videos">
                    <div class="resources-item-container" style="background-color: '.$resources_bg_color.';">
                        <div class="resource-type"><i class="fal fa-video"></i> '.$resources_type.'</div>
                        <h4 class="resource-title">'.$resources_title.'</h4>
                        <p>'.$body_full.'</p>
                        <div class="readmore-link video-link">
                            <a href="'.$resources_url.'"><i class="fal fa-play-circle"></i></a>
                        </div>
                    </div>
                </div>';
            endif;

            if( $resources_type == 'links' ): 
                echo '<div class="col-md-6 col-lg-6 col-xl-4 resources-item case-studies">
                    <div class="resources-item-container" style="background-image: url('.$resources_image_url.');">
                        <div class="resource-type"><i class="fal fa-link"></i> '.$resources_type.'</div>
                        <h4 class="resource-title">'.$resources_title.'</h4>
                        <p>'.$body_full.'</p>
                        <div class="readmore-link">
                            <a href="'.$resources_url.'">Read More <i class="fal fa-chevron-right"></i></a>
                            </div>
                    </div>
                </div>';
            endif;

            if( $resources_type == 'files' ):
                $type_color = ($resources_bg_color == "#BE955A") ? "resource-type-white": "";
                $title_color = ($resources_bg_color == "#BE955A") ? "resource-title-blue": "";
                $readmore_color = ($resources_bg_color == "#BE955A") ? "readmore-link-blue": "";

                echo '<div class="col-md-6 col-lg-6 col-xl-4 resources-item resources">
                    <div class="resources-item-container" style="background-color: '.$resources_bg_color.';">
                        <div class="resource-type '.$type_color.'"><i class="fal fa-file"></i> '.$resources_type.'</div>
                        <h4 class="resource-title '.$title_color.'">'.$resources_title.'</h4>
                        <p>'.$body_full.'</p>
                        <div class="readmore-link '.$readmore_color.'">
                            <a href="'.$resources_url.'">Read More <i class="fal fa-chevron-right"></i></a>
                            </div>
                    </div>
                </div>';
            endif;
        }
    }
}

function view_more_news($limit, $no_of_page, $date_filter){
    $start_pos = 0;
    $start_pos = ($no_of_page - 1) * $limit;

    $query = \Drupal::entityQuery('node');
	$query->condition('type', 'article');
    $query->condition('status', 1);
    $query->sort('created', 'DESC');
    
    if($cat_filter){
        $query->condition('field_news_category', $cat_filter);
    }

    if($date_filter){
        $query->condition('field_date', $date_filter . "%", 'LIKE');
    }

    $query->range($start_pos, $limit); 
	$nids = $query->execute();

    $nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

    if( $nodes ) {
		foreach( $nodes as $node ) {
            $news_date = $node->get('field_date')->getValue();

            if (isset($news_date[0]['value'])) {
                $newDate = date("F Y", strtotime($news_date[0]['value']));
                $news_date = $newDate;
            } else {
                $news_date = '';
            }

            $node_title = $node->get('title')->getValue();
            $news_title = isset($node_title[0]['value']) ? substr($node_title[0]['value'],0,50): '';
            
            $node_body = $node->get('body')->getValue();

            if (isset($node_body[0]['value'])) {
				$body_Data = strip_tags($node_body[0]['value']);
			} else {
				$body_Data = '';
			}

            if ($body_Data != '') {
				$body_full = $body_Data;
			} else {
				$body_full = '';
            }
            
            $news_url = $node->toUrl()->setAbsolute()->toString();

            $node_image = $node->get('field_image')->getValue();
			$news_image = [];
            
			if (isset($node_image[0])) {
				$file = File::load($node_image[0]['target_id']);
				$news_image_url = $file->url();
			} else {
				$news_image_url = '';
			}

            echo '<div class="col-12 col-lg-4 d-flex">
                        <div class="whh-text">
                        <span class="event-date text-uppercase">'.$news_date.'</span>
                        <h4>'.$news_title.'</h4>
                        <p class="d-none d-md-block">'.$body_full.'</p>
                        <div class="readmore-link">
                            <a href="'.$news_url.'">Read More <i class="fal fa-chevron-right"></i></a>
                        </div>
                        </div>
                        <div class="whh-img" style="background-image: url('.$news_image_url.');"></div>
                    </div>';
        }
    }
}

function view_more_faculty($limit, $no_of_page){
    $start_pos = 0;
    $start_pos = ($no_of_page - 1) * $limit;

    $query = \Drupal::entityQuery('node');
	$query->condition('type', 'faculty');
    $query->condition('status', 1);
    $query->sort('created', 'DESC');

    $query->range($start_pos, $limit); 
	$nids = $query->execute();

    $nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

    if( $nodes ) {
		foreach( $nodes as $node ) {
            $node_title = $node->get('title')->getValue();
            $faculty_title = isset($node_title[0]['value']) ? substr($node_title[0]['value'],0,50): '';
            
            $node_image = $node->get('field_image')->getValue();
            
			if (isset($node_image[0])) {
				$file = File::load($node_image[0]['target_id']);
                $faculty_image_url = $file->url();
                $faculty_img_alt = $node_image[0]['alt'];
			} else {
                $faculty_image_url = '';
                $faculty_img_alt = '';
            }
            
			$faculty_designation = $node->get('field_designation')->getValue();

            if (isset($faculty_designation[0]['value'])) {
				$faculty_designation = strip_tags($faculty_designation[0]['value']);
			} else {
				$faculty_designation = '';
			}

            echo '<div class="col-12 col-md-6 col-lg-4 col-xl-3 justify-content-center">
                    <div class="professor text-center">
                    <div class="professor-img">
                        <img src="'.$faculty_image_url.'" alt="'.$faculty_img_alt.'">
                    </div>
                    <div class="professor-info">
                        <h6 class="text-uppercase">'.$node_title.'<span class="professor-title d-block w-100">'.$faculty_designation.'</span></h6>
                    </div>
                    </div>
                </div>';
        }
    }
}

function view_more_career($limit, $no_of_page){
    $start_pos = 0;
    $start_pos = ($no_of_page - 1) * $limit;

    $query = \Drupal::entityQuery('node');
	$query->condition('type', 'careers');
    $query->condition('status', 1);
    $query->sort('created', 'DESC');

    $query->range($start_pos, $limit); 
	$nids = $query->execute();

    $nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

    if( $nodes ) {
		foreach( $nodes as $node ) {
            $node_title = $node->get('title')->getValue();
            $career_title = isset($node_title[0]['value']) ? substr($node_title[0]['value'],0,50): '';
            
            $node_pdf = $node->get('field_career_details_pdf')->getValue();
            
			if (isset($node_pdf[0])) {
				$file = File::load($node_pdf[0]['target_id']);
                $career_pdf = $file->createFileUrl();
			} else {
                $career_pdf = '';
            }
            
			$career_loc = $node->get('field_location')->getValue();

            if (isset($career_loc[0]['value'])) {
				$career_location = strip_tags($career_loc[0]['value']);
			} else {
				$career_location = '';
			}

            echo '<div class="col-md-4 careers-block-col">
                    <div class="careers-block">
                        <div class="job-sub-title">'.$career_location.'</div>
                        <h6 class="job-title">'.$career_title.'</h6>
                        <a href="'.$career_pdf.'" target="_blank"><i class="fal fa-file-pdf"></i> Download</a>
                    </div>
                </div>';
        }
    }
}

function view_more_events($limit, $no_of_page, $cat_filter){
    $start_pos = 0;
    $start_pos = ($no_of_page - 1) * $limit;

    $query = \Drupal::entityQuery('node');
	$query->condition('type', 'events');
    $query->condition('status', 1);
    $query->sort('field_date', 'DESC');
    
    if($cat_filter){
        $cats = explode(',', $cat_filter);
        $orConditions = $query->orConditionGroup();
        
        foreach($cats as $id) {
            $orConditions->condition('field_events_category', $id);
        }

        $query->condition($orConditions);
    }

    $query->range($start_pos, $limit); 
	$nids = $query->execute();

    $nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

    if( $nodes ) {
		foreach( $nodes as $node ) {
            $node_title = $node->get('title')->getValue();
            $event_title = isset($node_title[0]['value']) ? substr($node_title[0]['value'],0,50): '';
            
			$event_loc = $node->get('field_location')->getValue();

            if (isset($event_loc[0]['value'])) {
				$event_location = strip_tags($event_loc[0]['value']);
			} else {
				$event_location = '';
            }
            
            $events_map = $node->get('field_map_link')->getValue();

            if (isset($events_map[0]['value'])) {
				$map_Data = strip_tags($events_map[0]['value']);
			} else {
				$map_Data = '';
            }
            
            $event_time = $node->get('field_event_time')->getValue();

            if (isset($event_time[0]['value'])) {
				$event_time = strip_tags($event_time[0]['value']);
			} else {
				$events_time = '';
            }
            
            $events_date = $node->get('field_date')->getValue();

            if (isset($events_date[0]['value'])) {
                $newDate = date("F Y", strtotime($events_date[0]['value']));
                $events_date = $newDate;
                $calendarStartDate = date("Ymd", strtotime($events_date[0]['value']));
				$calendarEndDate = date("Ymd", strtotime("+1 day", strtotime($events_date[0]['value'])));
            } else {
                $events_date = '';
                $calendarStartDate = '';
				$calendarEndDate = '';
            }

            $node_body = $node->get('body')->getValue();

            if (isset($node_body[0]['value'])) {
				$body_Data = strip_tags($node_body[0]['value']);
			} else {
				$body_Data = '';
			}

            echo '<div class="col-12">
                    <div class="events-text">
                    <div class="events-calendar d-flex justify-content-between">
                        <span class="event-date text-uppercase">'.$events_date.'</span>
                        <span class="add-to-calendar d-none d-md-inline-block">
                        <a href="javascript:void(0)" class="dropdown-toggle line-bottom" role="button" id="dropdownMenuLink{{ event.nid }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fal fa-calendar"></i>
                          ADD TO CALENDAR
                        </a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuLink{{ event.nid }}">
                            <a class="dropdown-item" href="https://www.google.com/calendar/render?action=TEMPLATE&amp;text='.$event_title.'&amp;dates='.$calendarStartDate.'/'.$calendarEndDate.'&amp;details='.$body_Data.'&amp;location='.$event_location.'&amp;sprop=&amp;sprop=name:" target="_blank" rel="nofollow">Google Calendar</a>
                            <a class="dropdown-item" href="http://calendar.yahoo.com/?v=60&view=d&type=20&title='.$event_title.'&st='.$calendarStartDate.'&dur=&desc='.$body_Data.'&in_loc='.$event_location.'" target="_blank" rel="nofollow">Yahoo! Calendar</a>
                            <a class="dropdown-item" href="data:text/calendar;charset=utf8,BEGIN:VCALENDAR%0AVERSION:2.0%0ABEGIN:VEVENT%0AURL:http://carlsednaoui.github.io/add-to-calendar-buttons/example.html%0ADTSTART:'.$calendarStartDate.'%0ADTEND:'.$calendarEndDate.'%0ASUMMARY:'.$event_title.'%0ADESCRIPTION:'.$body_Data.'%0ALOCATION:'.$event_location.'%0AEND:VEVENT%0AEND:VCALENDAR" target="_blank" rel="nofollow">iCal Calendar</a>
                            <a class="dropdown-item" href="data:text/calendar;charset=utf8,BEGIN:VCALENDAR%0AVERSION:2.0%0ABEGIN:VEVENT%0AURL:http://carlsednaoui.github.io/add-to-calendar-buttons/example.html%0ADTSTART:'.$calendarStartDate.'%0ADTEND:'.$calendarEndDate.'%0ASUMMARY:'.$event_title.'%0ADESCRIPTION:'.$body_Data.'%0ALOCATION:'.$event_location.'%0AEND:VEVENT%0AEND:VCALENDAR" target="_blank" rel="nofollow">Outlook Calendar</a>
                            <a class="dropdown-item" href="https://addtocalendar.com/atc/ical?utz=330&uln=en-US&vjs=1.5&e[0][date_start]='.$calendarStartDate.'&e[0][date_end]='.$calendarEndDate.'&e[0][title]='.$event_title.'&e[0][description]='.$body_Data.'" target="_blank" rel="nofollow">Apple Calendar</a>
                        </div>
                        </span>
                    </div>
                    <div class="events-name">
                        <h4>'.$event_title.'</h4>
                    </div>
                    <div class="events-time">
                        <span class="e-time d-block">
                        <i class="fal fa-clock"></i>
                        '.$event_time.'
                        </span>
                        <span class="e-loc d-block">
                        <a target="_blank" href="'.$map_Data.'"><i class="fal fa-map-marker"></i>
                            '.$event_location.'</a>
                        </span>
                    </div>
                    <p class="event-content d-none d-md-block">
                        '.$body_Data.'
                    </p>
                    <a href="#" class="dropdown-toggle line-bottom d-md-none more-details" role="button" id="dropdownMenuLink{{ event.nid }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fal fa-calendar"></i>  
                    ADD TO CALENDAR
                    </a>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink{{ event.nid }}">
                        <a class="dropdown-item" href="https://www.google.com/calendar/render?action=TEMPLATE&amp;text='.$event_title.'&amp;dates='.$calendarStartDate.'/'.$calendarEndDate.'&amp;details='.$body_Data.'&amp;location='.$event_location.'&amp;sprop=&amp;sprop=name:" target="_blank" rel="nofollow">Google Calendar</a>
                        <a class="dropdown-item" href="http://calendar.yahoo.com/?v=60&view=d&type=20&title='.$event_title.'&st='.$calendarStartDate.'&dur=&desc='.$body_Data.'&in_loc='.$event_location.'" target="_blank" rel="nofollow">Yahoo! Calendar</a>
                        <a class="dropdown-item" href="data:text/calendar;charset=utf8,BEGIN:VCALENDAR%0AVERSION:2.0%0ABEGIN:VEVENT%0AURL:http://carlsednaoui.github.io/add-to-calendar-buttons/example.html%0ADTSTART:'.$calendarStartDate.'%0ADTEND:'.$calendarEndDate.'%0ASUMMARY:'.$event_title.'%0ADESCRIPTION:'.$body_Data.'%0ALOCATION:'.$event_location.'%0AEND:VEVENT%0AEND:VCALENDAR" target="_blank" rel="nofollow">iCal Calendar</a>
                        <a class="dropdown-item" href="data:text/calendar;charset=utf8,BEGIN:VCALENDAR%0AVERSION:2.0%0ABEGIN:VEVENT%0AURL:http://carlsednaoui.github.io/add-to-calendar-buttons/example.html%0ADTSTART:'.$calendarStartDate.'%0ADTEND:'.$calendarEndDate.'%0ASUMMARY:'.$event_title.'%0ADESCRIPTION:'.$body_Data.'%0ALOCATION:'.$event_location.'%0AEND:VEVENT%0AEND:VCALENDAR" target="_blank" rel="nofollow">Outlook Calendar</a>
                        <a class="dropdown-item" href="https://addtocalendar.com/atc/ical?utz=330&uln=en-US&vjs=1.5&e[0][date_start]='.$calendarStartDate.'&e[0][date_end]='.$calendarEndDate.'&e[0][title]='.$event_title.'&e[0][description]='.$body_Data.'" target="_blank" rel="nofollow">Apple Calendar</a>
                    </div>
                    </div>
                </div>';
        }
    }
}