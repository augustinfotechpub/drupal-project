<?php // HEC custom functions
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\taxonomy\Entity\Term;

use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\Core\Url;

use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\block\Entity\Block;
use Drupal\Core\Database\Database;

// get node details by node ID
function getNodeDetailsByID($nid, $bundle) {
	$query = \Drupal::entityQuery('node');
	$query->condition('type', $bundle);
	$query->condition('status', 1);
	$query->condition('nid' , $nid); 
	$node_data = $query->execute();

	$nodes_data = \Drupal\node\Entity\Node::loadMultiple($node_data);
	
	$node_array = [];
	if( $nodes_data ) {
		foreach( $nodes_data as $node ) {
			$node_array['block_title'] = $node->get('title')->getValue();
			$node_body = $node->get('body')->getValue();

			if (isset($node_body[0]['value'])) {
				$node_array['block_content'] = $node_body[0]['value'];
			} else {
				$node_array['block_content'] = '';
			}

			$node_url = $node->toUrl()->setAbsolute()->toString();
			$node_array['block_url'] = $node_url;

			if($bundle == 'testimonials'){
				$testimonial_qual = $node->get('field_qualification')->getValue();

				if (isset($testimonial_qual[0]['value'])) {
					$node_array['qualification'] = strip_tags($testimonial_qual[0]['value']);
				} else {
					$node_array['qualification'] = '';
				}
			}

			if($bundle == 'programs'){
				$node_image = $node->get('field_banner_image')->getValue();
				$block_image = [];

				if (isset($node_image[0])) {
					$file = File::load($node_image[0]['target_id']);
					$block_image['url'] = $file->url();
					$block_image['alt'] = $node_image[0]['alt'];
				} else {
					$block_image['url'] = '';
					$block_image['alt'] = '';
				}

				$node_array['block_image'] = $block_image;
			}

			if($bundle == 'page'){
				$node_image = $node->get('field_thumbnail_image')->getValue();
				$block_image = [];

				if (isset($node_image[0])) {
					$file = File::load($node_image[0]['target_id']);
					$block_image['url'] = $file->url();
					$block_image['alt'] = $node_image[0]['alt'];
				} else {
					$block_image['url'] = '';
					$block_image['alt'] = '';
				}

				$node_array['block_image'] = $block_image;
			}

			if($bundle == 'testimonials'){
				$node_image = $node->get('field_image')->getValue();
				$block_image = [];

				if (isset($node_image[0])) {
					$file = File::load($node_image[0]['target_id']);
					$block_image['url'] = $file->url();
					$block_image['alt'] = $node_image[0]['alt'];
				} else {
					$block_image['url'] = '';
					$block_image['alt'] = '';
				}

				$node_array['block_image'] = $block_image;
			}
		}
	}

	return $node_array;
}

// get programs list
function getPagesList() {	
	$node_array = [];

	$query = \Drupal::entityQuery('node');
	$query->condition('type', 'page');
    $query->condition('status', 1);
    $query->sort('created' , 'DESC'); 
	$nids = $query->execute();

	$nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

	$node_array = [];
	if( $nodes ) {
		foreach( $nodes as $node ) {
			$node_id = $node->get('nid')->getValue();
			$programs_array = array();
			$program_id = $node_id[0]['value'];

			$node_title = $node->get('title')->getValue();
			$program_title = $node_title[0]['value'];
			
			$node_array[$program_id] = $program_title;
		}
    }
	
    return $node_array;
}

// get programs list
function getProgramsList($limit) {	
	$node_array = [];

	$query = \Drupal::entityQuery('node');
	$query->condition('type', 'programs');
    $query->condition('status', 1);
	$query->sort('created' , 'DESC'); 
	$query->condition('field_display_on_homepage', 1);
	if( $limit ) {
		$query->range(0,$limit);
	}
	$nids = $query->execute();

	$nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

	$node_array = [];
	if( $nodes ) {
		foreach( $nodes as $node ) {
			
			$node_id = $node->get('nid')->getValue();
			$programs_array = array();
			$programs_array['nid'] = $node_id[0]['value'];

			$node_title = $node->get('title')->getValue();
			$programs_array['title'] = $node_title[0]['value'];

			$node_url = $node->toUrl()->setAbsolute()->toString();
			$programs_array['program_url'] = $node_url;
			
			$node_image = $node->get('field_thumbnail_image')->getValue();
			$program_image = [];

			if (isset($node_image[0])) {
				$file = File::load($node_image[0]['target_id']);
				$program_image['url'] = $file->url();
				$program_image['alt'] = $node_image[0]['alt'];
			} else {
				$program_image['url'] = '';
				$program_image['alt'] = '';
			}
			$programs_array['program_image'] = $program_image;
			
			$node_array[] = $programs_array;
		}
    }
	
    return $node_array;
}

// get leaders and department heads list
function getLeadersAndDepartmentHeadsList() {	
	$node_array = [];

	$query = \Drupal::entityQuery('node');
	$query->condition('type', 'leaders_department_heads');
    $query->condition('status', 1);
	$query->sort('created' , 'DESC'); 
	$query->range(0, 4);
	$nids = $query->execute();

	$nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

	$node_array = [];
	if( $nodes ) {
		foreach( $nodes as $node ) {
			
			$node_id = $node->get('nid')->getValue();
			$ld_array = array();
			$ld_array['nid'] = $node_id[0]['value'];

			$node_title = $node->get('title')->getValue();
			$ld_array['title'] = $node_title[0]['value'];
			
			$node_image = $node->get('field_image')->getValue();
			$ld_image = [];

			if (isset($node_image[0])) {
				$file = File::load($node_image[0]['target_id']);
				$ld_image['url'] = $file->url();
				$ld_image['alt'] = $node_image[0]['alt'];
			} else {
				$ld_image['url'] = '';
				$ld_image['alt'] = '';
			}
			$ld_array['leader_image'] = $ld_image;

			$ld_designation = $node->get('field_designation')->getValue();

            if (isset($ld_designation[0]['value'])) {
				$ld_array['designation'] = strip_tags($ld_designation[0]['value']);
			} else {
				$ld_array['designation'] = '';
			}
			
			$node_array[] = $ld_array;
		}
    }
	
    return $node_array;
}

// get events list
function getEventsList($limit) {	
	$node_array = [];

	$query = \Drupal::entityQuery('node');
	$query->condition('type', 'events');
	$query->condition('status', 1);
	$query->sort('field_date', 'DESC');
	
	if( $limit ) {
		$query->range(0,$limit);
	}

	$nids = $query->execute();

	$nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

	$node_array = [];
	if( $nodes ) {
		foreach( $nodes as $node ) {
			
			$node_id = $node->get('nid')->getValue();
			$events_array = array();
			$events_array['nid'] = $node_id[0]['value'];

			$node_title = $node->get('title')->getValue();
			$events_array['title'] = $node_title[0]['value'];

            $node_body = $node->get('body')->getValue();

            if (isset($node_body[0]['value'])) {
				$body_Data = $node_body[0]['value'];
			} else {
				$body_Data = '';
			}

            if ($body_Data != '') {
				$events_array['body_full'] = $body_Data;
			} else {
				$events_array['body_full'] = '';
			}

			$events_category = $node->get('field_events_category')->getValue();
			
			$events_date = $node->get('field_date')->getValue();

            if (isset($events_date[0]['value'])) {
				$newDate = date("l, d F Y", strtotime($events_date[0]['value']));
				$calendarStartDate = date("Ymd", strtotime($events_date[0]['value']));
				$calendarEndDate = date("Ymd", strtotime("+1 day", strtotime($events_date[0]['value'])));
				$events_array['events_date'] = $newDate;
				$events_array['events_start_date'] = $calendarStartDate;
				$events_array['events_end_date'] = $calendarEndDate;
            } else {
				$events_array['events_date'] = '';
				$events_array['events_start_date'] = '';
				$events_array['events_end_date'] = '';
            }

			$node_url = $node->toUrl()->setAbsolute()->toString();
			
			$events_location = $node->get('field_location')->getValue();

            if (isset($events_location[0]['value'])) {
				$location_Data = strip_tags($events_location[0]['value']);
			} else {
				$location_Data = '';
			}

			$event_time = $node->get('field_event_time')->getValue();

            if (isset($event_time[0]['value'])) {
				$events_array['event_time'] = strip_tags($event_time[0]['value']);
			} else {
				$events_array['event_time'] = '';
			}

            if ($location_Data != '') {
				$events_array['event_location'] = $location_Data;
			} else {
				$events_array['event_location'] = '';
			}

            $events_map = $node->get('field_map_link')->getValue();

            if (isset($events_map[0]['value'])) {
				$map_Data = strip_tags($events_map[0]['value']);
			} else {
				$map_Data = '';
			}

            if ($map_Data != '') {
				$events_array['event_map'] = $map_Data;
			} else {
				$events_array['event_map'] = '';
			}
		
			$node_array[] = $events_array;
		}
    }
	
    return $node_array;
}

// get all events count
function getAllEventsCount(){
	$query = \Drupal::entityQuery('node');
	$query->condition('type', 'events');
    $query->condition('status', 1);
    $query->sort('created' , 'DESC'); 
	$nids = $query->execute();

	return count($nids);
}

// get all resources count
function getResourcesCount(){
	$query = \Drupal::entityQuery('node');
	$query->condition('type', 'resources');
    $query->condition('status', 1);
    $query->sort('created' , 'DESC'); 
	$nids = $query->execute();

	return count($nids);
}

// get faculty list
function getFacultiesData($limit) {	
	$node_array = [];

	$query = \Drupal::entityQuery('node');
	$query->condition('type', 'faculty');
	$query->condition('status', 1);
	$query->sort('created' , 'DESC'); 
	
	if( $limit ) {
		$query->range(0,$limit);
	}

	$nids = $query->execute();

	$nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

	$node_array = [];
	if( $nodes ) {
		foreach( $nodes as $node ) {
			
			$node_id = $node->get('nid')->getValue();
			$faculty_array = array();
			$faculty_array['nid'] = $node_id[0]['value'];

			$node_title = $node->get('title')->getValue();
			$faculty_array['title'] = $node_title[0]['value'];
			
			$faculty_designation = $node->get('field_designation')->getValue();

            if (isset($faculty_designation[0]['value'])) {
				$faculty_array['designation'] = strip_tags($faculty_designation[0]['value']);
			} else {
				$faculty_array['designation'] = '';
			}

			$node_image = $node->get('field_image')->getValue();
			$faculty_image = [];
            
			if (isset($node_image[0])) {
				$file = File::load($node_image[0]['target_id']);
				$faculty_image['url'] = $file->url();
				$faculty_image['alt'] = $node_image[0]['alt'];
			} else {
				$faculty_image['url'] = '';
				$faculty_image['alt'] = '';
			}
            
			$faculty_array['faculty_image'] = $faculty_image;
		
			$node_array[] = $faculty_array;
		}
    }
	
    return $node_array;
}

// get all faculty count
function getAllFacultyCount(){
	$query = \Drupal::entityQuery('node');
	$query->condition('type', 'faculty');
    $query->condition('status', 1);
    $query->sort('created' , 'DESC'); 
	$nids = $query->execute();

	return count($nids);
}

// get careers list
function getCareersData($limit) {	
	$node_array = [];

	$query = \Drupal::entityQuery('node');
	$query->condition('type', 'careers');
	$query->condition('status', 1);
	$query->sort('created' , 'DESC'); 
	
	if( $limit ) {
		$query->range(0,$limit);
	}

	$nids = $query->execute();

	$nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

	$node_array = [];
	if( $nodes ) {
		foreach( $nodes as $node ) {
			
			$node_id = $node->get('nid')->getValue();
			$careers_array = array();
			$careers_array['nid'] = $node_id[0]['value'];

			$node_title = $node->get('title')->getValue();
			$careers_array['title'] = $node_title[0]['value'];
			
			$career_location = $node->get('field_location')->getValue();

            if (isset($career_location[0]['value'])) {
				$careers_array['location'] = strip_tags($career_location[0]['value']);
			} else {
				$careers_array['location'] = '';
			}

			$node_pdf = $node->get('field_career_details_pdf')->getValue();
			$career_pdf = [];
            
			if (isset($node_pdf[0])) {
				$file = File::load($node_pdf[0]['target_id']);
				$career_pdf['url'] = $file->createFileUrl();
			} else {
				$career_pdf['url'] = '';
			}
            
			$careers_array['career_pdf'] = $career_pdf;
		
			$node_array[] = $careers_array;
		}
    }
	
    return $node_array;
}

// get careers count
function getCareersCount(){
	$query = \Drupal::entityQuery('node');
	$query->condition('type', 'careers');
    $query->condition('status', 1);
    $query->sort('created' , 'DESC'); 
	$nids = $query->execute();

	return count($nids);
}

// get all news count
function getAllNewsCount(){
	$query = \Drupal::entityQuery('node');
	$query->condition('type', 'article');
    $query->condition('status', 1);
    $query->sort('created' , 'DESC'); 
	$nids = $query->execute();

	return count($nids);
}

// get news data
function getNewsData($limit) {	
	$node_array = [];

	$query = \Drupal::entityQuery('node');
	$query->condition('type', 'article');
    $query->condition('status', 1);
    $query->sort('created' , 'DESC'); 
	
	if( $limit ) {
		$query->range(0,$limit);
	}

	$nids = $query->execute();

	$nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

	$node_array = [];
	if( $nodes ) {
		foreach( $nodes as $node ) {
			
			$node_id = $node->get('nid')->getValue();
			$news_array = array();
			$news_array['nid'] = $node_id[0]['value'];

			$node_title = $node->get('title')->getValue();
			$news_array['title'] = $node_title[0]['value'];

			$node_url = $node->toUrl()->setAbsolute()->toString();
			$news_array['news_url'] = $node_url;
			
			$node_image = $node->get('field_image')->getValue();
			$news_image = [];
            
			if (isset($node_image[0])) {
				$file = File::load($node_image[0]['target_id']);
				$news_image['url'] = $file->url();
				$news_image['alt'] = $node_image[0]['alt'];
			} else {
				$news_image['url'] = '';
				$news_image['alt'] = '';
			}
            
			$news_array['news_image'] = $news_image;

            $node_body = $node->get('body')->getValue();

            $news_date = $node->get('field_date')->getValue();
			$news_dates = [];

            if (isset($news_date[0]['value'])) {
                $newDate = date("F Y", strtotime($news_date[0]['value']));
				$newsDateFilter = date("Y-m", strtotime($news_date[0]['value']));
                $news_array['news_date'] = $newDate;
				$news_dates[$newsDateFilter] = $newDate;
            } else {
                $news_array['news_date'] = '';
            }

			$news_array['dates_filter'] = $news_dates;

            if (isset($node_body[0]['value'])) {
				$body_Data = strip_tags($node_body[0]['summary']);
			} else {
				$body_Data = '';
			}

            if ($body_Data != '') {
				$news_array['body_full'] = substr($body_Data, 0, 200);
			} else {
				$news_array['body_full'] = '';
			}
			
			$node_array[] = $news_array;
		}
    }
	
    return $node_array;
}

// get resources data
function getResourcesData($limit) {	
	$node_array = [];

	$query = \Drupal::entityQuery('node');
	$query->condition('type', 'resources');
    $query->condition('status', 1);
    $query->sort('created' , 'DESC'); 
	
	if( $limit ) {
		$query->range(0,$limit);
	}

	$nids = $query->execute();

	$nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

	$node_array = [];
	if( $nodes ) {
		foreach( $nodes as $node ) {
			
			$node_id = $node->get('nid')->getValue();
			$resources_array = array();
			$resources_array['nid'] = $node_id[0]['value'];

			$node_title = $node->get('title')->getValue();
			$resources_array['title'] = $node_title[0]['value'];

			$node_url = $node->toUrl()->setAbsolute()->toString();
			$resources_array['resources_url'] = $node_url;
			
			$node_image = $node->get('field_image')->getValue();
			$resources_image = [];
            
			if (isset($node_image[0])) {
				$file = File::load($node_image[0]['target_id']);
				$resources_image['url'] = $file->url();
				$resources_image['alt'] = $node_image[0]['alt'];
			} else {
				$resources_image['url'] = '';
				$resources_image['alt'] = '';
			}
            
			$resources_array['resources_image'] = $resources_image;

            $node_body = $node->get('body')->getValue();

            if (isset($node_body[0]['value'])) {
				$body_Data = strip_tags($node_body[0]['summary']);
			} else {
				$body_Data = '';
			}

			$node_resources_type = $node->get('field_resources_type')->getValue();

            if (isset($node_resources_type[0]['value'])) {
				$resources_array['resources_type'] = $node_resources_type[0]['value'];
			} else {
				$resources_array['resources_type'] = '';
			}

			$node_bg_color = $node->get('field_block_background_color')->getValue();

            if (isset($node_bg_color[0]['color'])) {
				$resources_array['resources_bg_color'] = $node_bg_color[0]['color'];
			} else {
				$resources_array['resources_bg_color'] = '#003671';
			}

            if ($body_Data != '') {
				$resources_array['body_full'] = substr($body_Data, 0, 200);
			} else {
				$resources_array['body_full'] = '';
			}
			
			$node_array[] = $resources_array;
		}
    }
	
    return $node_array;
}

// get news dates
function getNewsDates() {	
	$node_array = [];

	$query = \Drupal::entityQuery('node');
	$query->condition('type', 'article');
    $query->condition('status', 1);
    $query->sort('created' , 'DESC'); 

	$nids = $query->execute();

	$nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

	$node_array = [];
	if( $nodes ) {
		foreach( $nodes as $node ) {
			$news_date = $node->get('field_date')->getValue();
			$news_dates = [];

            if (isset($news_date[0]['value'])) {
				$newDate = date("F Y", strtotime($news_date[0]['value']));
				$newsDateFilter = date("Y-m", strtotime($news_date[0]['value']));
				$node_array[$newsDateFilter] = $newDate;
            }
		}
    }
	
    return $node_array;
}

// get news terms list
function getNewsTerms(){
	$vid = 'news_category';
	$get_terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
	$terms = array();

	foreach ($get_terms as $term) {
		$terms[$term->tid] = $term->name;
	}
	
	ksort($terms);

	return $terms;
}

// get events terms list
function getEventsTerms(){
	$vid = 'events_category';
	$get_terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
	$terms = array();

	foreach ($get_terms as $term) {
		$terms[$term->tid] = $term->name;
	}
	
	return $terms;
}

// get resources terms list
function getResourcesTerms(){
	$vid = 'resources_category';
	$get_terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
	$terms = array();

	foreach ($get_terms as $term) {
		$terms[$term->tid] = $term->name;
	}
	
	return $terms;
}

// get news next and previous link
function getNextPreviousByNodeID($findNodeId, $bundle) {
	$query = \Drupal::entityQuery('node');
	$query->condition('type', $bundle);
    $query->condition('status', 1);
    $query->sort('created' , 'DESC'); 

	$nids = $query->execute();

	$nodes = \Drupal\node\Entity\Node::loadMultiple($nids);

	$news_array = [];
	if( $nodes ) {
		foreach( $nodes as $node ) {
			$node_id = $node->get('nid')->getValue();
			$news_array[$node_id[0]['value']] = $node->toUrl()->setAbsolute()->toString();
		}
	}

	$nav_links = array();

	$keys = array_keys($news_array);
    $position = array_search($findNodeId, $keys);
    if (isset($keys[$position + 1])) {
        $nextKey = $keys[$position + 1];
		if($nextKey){
			$nav_links['next_link'] = $news_array[$nextKey];
		}
    }

    $position = array_search($findNodeId, $keys);
    if (isset($keys[$position - 1])) {
        $prevKey = $keys[$position - 1];
		if($prevKey){
			$nav_links['prev_link'] = $news_array[$prevKey];
		}
    }

    return $nav_links;
}