jQuery( document ).ready(function($) {
  $(function () {
    setNavigation();
  });

  function setNavigation() {
    var path = window.location.pathname;
    path = path.replace(/\/$/, "");
    path = decodeURIComponent(path);

    $(".nav-item a").each(function () {
        var href = $(this).attr('href');
        if (path.substring(0, href.length) === href) {
            $(this).closest('li').addClass('active');
        }
    });

    $(".quick-links .links > a").each(function () {
        var href = $(this).attr('href');
        if (path.substring(0, href.length) === href) {
            $(this).addClass('active');
        }
    });
  }
  if (window.matchMedia("(min-width: 992px)").matches) {
    var sections = $('.section');
    var nav = $('nav.navbar');
    var nav_height = nav.innerHeight();
  
    $(window).on('scroll', function () {
      var cur_pos = $(this).scrollTop();
      
      sections.each(function() {
        var top = $(this).offset().top - nav_height,
        bottom = top + $(this).outerHeight();
        
        if (cur_pos >= top && cur_pos <= bottom) {
          nav.find('a[data-scroll]').removeClass('active');
          sections.removeClass('active');
          
          $(this).addClass('active');
          nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
        }
      });
    });

    nav.find('a[data-scroll]').on('click', function () {
      var $el = $(this)
        , id = $el.attr('href');
      // $('html, body').animate({
      //   // scrollTop: $(id).offset().top - nav_height
      //   scrollTop: $($(this).attr('href')).offset().top,
      // }, 1000, 'linear');
      scrollToAnimate(id);
      
      return false;
    });
  }

  if (window.matchMedia("(max-width: 991px)").matches) {
    var sections = $('.section');
    var nav = $('.ste-header .navbar-toggler');
    var nav_height = nav.innerHeight();
  
    $(window).on('scroll', function () {
      var cur_pos = $(this).scrollTop();
      
      sections.each(function() {
        var top = $(this).offset().top - nav_height,
        bottom = top + $(this).outerHeight();
        
        if (cur_pos >= top && cur_pos <= bottom) {
          nav.find('a[data-scroll]').removeClass('active');
          sections.removeClass('active');
          
          $(this).addClass('active');
          nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
        }
      });
    });

    nav.find('a[data-scroll]').on('click', function (e) {
      var $el = $(this)
        , id = $el.attr('href');
      // $('html, body').animate({
      //   // scrollTop: $(id).offset().top - nav_height
      //   scrollTop: $($(this).attr('href')).offset().top,
      // }, 1000, 'linear');
      nav.find('a[data-scroll]').removeClass('active');
      console.log('hi');
      $(this).addClass('active');
      scrollToAnimate(id);
      return false;
    });
  }
  
  

  if (window.matchMedia("(max-width: 991px)").matches) {
    $('.navbar a[data-scroll]').on('click', function(){
        $('#navbarNavMobile').collapse('hide');
        $("body").removeClass("navbar-show");
    });
  }

  $("html:not([dir='rtl']) .achievements-slider").owlCarousel({
    loop: true,
    margin: 0,
    nav: true,
    dots: true,
    navText: [
      '<i class="fal fa-chevron-left"></i>',
      '<i class="fal fa-chevron-right"></i>'
    ],
    responsive:{
        0:{
            items: 1,
            nav: false,
        },
        768:{
            items: 2
        },
        1000:{
            items: 3
        }
    }
  });

  $("html[dir='rtl'] .achievements-slider").owlCarousel({
    rtl:true,
    loop: true,
    margin: 0,
    nav: true,
    dots: true,
    navText: [
      '<i class="fal fa-chevron-left"></i>',
      '<i class="fal fa-chevron-right"></i>'
    ],
    responsive:{
        0:{
            items: 1,
            nav: false,
        },
        768:{
            items: 2
        },
        1000:{
            items: 3
        }
    }
  });

  if (window.matchMedia("(min-width: 992px)").matches) {
    if($(".sec-sitemap-left-inner").length) {
      
      $(".sec-sitemap-left-inner").mCustomScrollbar({
        mouseWheel:{
          enable:true,
          scrollAmount:"auto",
          axis:"y",
          preventDefault:true,
        },
        contentTouchScroll:25,
        setWidth:false,
        setHeight:false,
        setTop:0,
        setLeft:0,
        axis:"y",
      });

    }
  }

  if (window.matchMedia("(min-width: 768px)").matches) {

    $("html:not([dir='rtl']) .delivery-process-slider").owlCarousel({
      nav: false,
      dots: false,
      autoWidth:true,
      items: 3,
      navText: [
        '<i class="fal fa-chevron-left"></i>',
        '<i class="fal fa-chevron-right"></i>'
      ],
      responsive:{
          0:{
              items: 1,
              nav: false,
          },
          768:{
            margin: 50,
          },
          1200:{
            margin: 135,
          }
      }
    });

    $("html[dir='rtl'] .delivery-process-slider").owlCarousel({
      rtl:true,
      nav: false,
      dots: false,
      autoWidth:true,
      items: 3,
      navText: [
        '<i class="fal fa-chevron-left"></i>',
        '<i class="fal fa-chevron-right"></i>'
      ],
      responsive:{
          0:{
              items: 1,
              nav: false,
          },
          768:{
            margin: 50,
          },
          1200:{
            margin: 135,
          }
      }
    });
    

  }

  /*if($('#resources').length) {
    var $container = $('#resources').isotope({
      itemSelector : '.resources-item',
      isFitWidth: true,
      layoutMode: 'fitRows',
      resizable: false, // disable normal resizing
    });
    
    
    $container.isotope({ filter: '*' });

    if (window.matchMedia("(min-width: 768px)").matches) {
      // filter items on button click
      $('#resourcesFiltersDesktop').on( 'click', 'button', function() {
        $('#resourcesFiltersDesktop button').removeClass("active");
        $(this).addClass("active"); 
        var filterValue = $(this).attr('data-filter');
        $container.isotope({ filter: filterValue });

      });
    }

    if (window.matchMedia("(max-width: 767px)").matches) {
      var getValue = $('.resources-dropdown-menu li[selected]').html();
      $('.dropdown-select').html(getValue);

      $('.resources-dropdown-menu li').on('click', function() {
        
        $('.resources-dropdown-menu li').removeClass("active");
        $(this).addClass("active"); 
        // var getValue = $(this).text();
        var getValue = $(this).html();
        // $('.dropdown-select').text(getValue);
        $('.dropdown-select').html(getValue);
        var filterValue = $(this).attr('data-filter');
        $container.isotope({ filter: filterValue });
      });
    }
      
  }*/

  // var classNames = [];
  // if (navigator.userAgent.match(/(iPad|iPhone|iPod)/i)) classNames.push('device-ios');
  // if (navigator.userAgent.match(/android/i)) classNames.push('device-android');

  // var body = document.getElementsByTagName('body')[0];

  // if (classNames.length) classNames.push('on-device');
  // if (body.classList) body.classList.add.apply(body.classList, classNames);

  if (navigator.userAgent.indexOf('Mac OS X') != -1) {
    $("body").addClass("mac");
  }

  if(navigator.userAgent.indexOf("MSIE")!=-1 || navigator.userAgent.indexOf("rv:11.0")!=-1) $("body").addClass("msie");

  else if(navigator.userAgent.indexOf("Edge")!=-1) $("body").addClass("edge");

  else if(navigator.userAgent.indexOf("Firefox")!=-1) $("body").addClass("firefox");

  else if(navigator.userAgent.indexOf("Opera")!=-1) $("body").addClass("opera");

  else if(navigator.userAgent.indexOf("Chrome") != -1) $("body").addClass("chrome");

 else if(navigator.userAgent.indexOf("Safari")!=-1) $("body").addClass("safari");


    
});

/* [Scroll to section by Hash/Id] */
window.scrollToAnimate = function (hash) {
  // console.log(hash);
  if (hash != null && hash != undefined) {
    var scrollPos = $(hash).offset().top;
    var offset = $(".navbar.fixed-top").outerHeight();
    // $(window).scrollTop(scrollPos - offset);
    $('html, body').animate({
      scrollTop: scrollPos - offset
    }, 600, function(){
      // Add hash (#) to URL when done scrolling (default click behavior)
      // window.location.hash = hash;
    });
  }
}

function enableclick(){
  //enable click
}

function LoadMoreNews(){
  page = page+1;

  var cat_filter = $('.news_date_cat_filter').data('news_cat_filter');
  var date_filter = $('.news_date_cat_filter').data('news_date_filter');

  $('.loader').css('display', 'block');
  $.post(location.protocol + "/load_ajax_data", { 'action': 'view_more_news', 'limit': limit, 'no_of_page': page, 'cat_filter': cat_filter, 'date_filter': date_filter }, function(data){
    $('.loader').css('display', 'none'); 

    if(data){
      $('#news-listing').append(data);
    } else {
      $('.press-loading-example-btn').prop("onclick", null).off("click");
      var html ='<div class="col-12 col-lg-12 d-flex text-center" id="norecord">'
				  +'<div class="noval alert alert-danger w-100 mb-5" id="norecord">No Record Found</div>'
        +'</div>';
        $('#news-listing').append(html);
    }

  });
}

function LoadMoreNewsFilter(page){
  page = page+1;

  var cat_filter = $('.news_date_cat_filter').data('news_cat_filter');
  var date_filter = $('.news_date_cat_filter').data('news_date_filter');

  $('.loader').css('display', 'block');
  $.post(location.protocol + "/load_ajax_data", { 'action': 'view_more_news', 'limit': limit, 'no_of_page': page, 'cat_filter': cat_filter, 'date_filter': date_filter }, function(data){
    $('.loader').css('display', 'none'); 

    if(data){
      $('#news-listing').append(data);
    } else {
      $('.press-loading-example-btn').prop("onclick", null).off("click");
      var html ='<div class="col-12 col-lg-12 d-flex text-center" id="norecord">'
				  +'<div class="noval alert alert-danger w-100 mb-5" id="norecord">No Record Found</div>'
        +'</div>';
        $('#news-listing').append(html);
    }

  });
}

$('body.page-node-type-article .site-header, body.login-page .site-header, body.page-node-type-resources .site-header, body.page-not-found .site-header, body.page-node-35 .site-header, body.page-node-63 .site-header').addClass('site-sub-header site-header-gray site-logo-blue');

$('.news_cat').click(function(){
  $('.press-loading-example-btn').on('click', enableclick);
  var news_cat = $(this).data('news_cat');
  var news_cat_txt = $(this).data('news_cat_txt');
  var page = 0;

  $('.selected-cat').text(news_cat_txt);
  $('.news_date_cat_filter').data('news_cat_filter', news_cat);
  $('#news-listing').html('');

  LoadMoreNewsFilter(page);
});

$('.news_date').click(function(){
  $('.press-loading-example-btn').on('click', enableclick);
  var news_dates = $(this).data('news_dates');
  var news_dates_txt = $(this).data('news_date_txt');
  var page = 0;

  $('.right-date').text(news_dates_txt);
  $('.selected-date').text(news_dates_txt);

  $('#news-listing').html('');
  $('.news_date_cat_filter').data('news_date_filter', news_dates);

  LoadMoreNewsFilter(page);
});

$('.page-node-type-article .site-logo-blue .navbar-brand a, .login-page .site-logo-blue .navbar-brand a, .page-node-type-resources .site-logo-blue .navbar-brand a, .page-not-found .site-logo-blue .navbar-brand a').append('<img src="/themes/custom-themes/hec/assets/images/hec-logo-2.svg" alt="Home">');

function LoadMoreFaculty(){
  page = page+1;

  $('.loader').css('display', 'block');
  $.post(location.protocol + "/load_ajax_data", { 'action': 'view_more_faculty', 'limit': limit, 'no_of_page': page }, function(data){
    $('.loader').css('display', 'none'); 

    if(data){
      $('#faculty-listing').append(data);
    } else {
      $('.press-loading-example-btn').prop("onclick", null).off("click");
      var html ='<div class="col-12 col-lg-12 d-flex text-center" id="norecord">'
				  +'<div class="noval alert alert-danger w-100 mt-5" id="norecord">No Record Found</div>'
        +'</div>';
        $('#faculty-listing').append(html);
    }

  });
}

function LoadMoreCareers(){
  page = page+1;

  $('.loader').css('display', 'block');
  $.post(location.protocol + "/load_ajax_data", { 'action': 'view_more_career', 'limit': limit, 'no_of_page': page }, function(data){
    $('.loader').css('display', 'none'); 

    if(data){
      $('#careers-listing').append(data);
    } else {
      $('.press-loading-example-btn').prop("onclick", null).off("click");
      var html ='<div class="col-12 col-lg-12 d-flex text-center mt-5" id="norecord">'
				  +'<div class="noval alert alert-danger w-100" id="norecord">No Record Found</div>'
        +'</div>';
        $('#careers-listing').append(html);
    }

  });
}

function LoadMoreEvents(){
  page = page+1;
  var cat_filter = $('.events-cat-filter').data('events_cat_filter');

  $('.loader').css('display', 'block');
  $.post(location.protocol + "/load_ajax_data", { 'action': 'view_more_events', 'limit': limit, 'no_of_page': page, 'cat_filter': cat_filter }, function(data){
    $('.loader').css('display', 'none'); 

    if(data){
      $('#events-listing').append(data);
    } else {
      $('.press-loading-example-btn').prop("onclick", null).off("click");
      var html ='<div class="col-12 col-lg-12 d-flex text-center mt-3" id="norecord">'
				  +'<div class="noval alert alert-danger w-100 mb-5" id="norecord">No Record Found</div>'
        +'</div>';
        $('#events-listing').append(html);
    }

  });
}

function LoadMoreEventsFilter(page){
  page = page+1;

  var cat_filter = $('.events-cat-filter').data('events_cat_filter');

  $('.loader').css('display', 'block');
  $.post(location.protocol + "/load_ajax_data", { 'action': 'view_more_events', 'limit': limit, 'no_of_page': page, 'cat_filter': cat_filter }, function(data){
    $('.loader').css('display', 'none'); 

    if(data){
      $('#events-listing').append(data);
    } else {
      $('.press-loading-example-btn').prop("onclick", null).off("click");
      var html ='<div class="col-12 text-center mt-3" id="norecord">'
				  +'<div class="noval alert alert-danger w-100 mb-5" id="norecord">No Record Found</div>'
        +'</div>';
        $('#events-listing').append(html);
    }

  });
}

$('.events_filters').click(function(){
  $('.press-loading-example-btn').on('click', enableclick);
  var events_cat = $('.events_filters:checked').map(function() {return this.value;}).get().join(',');

  var page = 0;

  $('.events-cat-filter').data('events_cat_filter', events_cat);
  $('#events-listing').html('');

  LoadMoreEventsFilter(page);
});

$('.clear_events_filters').click(function(){
  $('.press-loading-example-btn').on('click', enableclick);
  $('.events_filters').each(function() {
    this.checked = false;
  });

  var page = 0;

  $('.events-cat-filter').data('events_cat_filter', '');
  $('#events-listing').html('');

  LoadMoreEventsFilter(page);
});

function LoadMoreResources(){
  page = page+1;
  var cat_filter = $('.resources_cat_filter').data('resources_cat_filter');
  var search_keyword = $('.resources_cat_filter').data('resources_search_keyword');

  $('.loader').css('display', 'block');
  $.post(location.protocol + "/load_ajax_data", { 'action': 'view_more_resources', 'limit': limit, 'no_of_page': page, 'cat_filter': cat_filter, 'search_keyword': search_keyword }, function(data){
    $('.loader').css('display', 'none'); 

    if(data){
      $('.resources-listing').append(data);
    } else {
      $('.press-loading-example-btn').prop("onclick", null).off("click");
      var html ='<div class="col-12 col-lg-12 d-flex text-center mt-3" id="norecord">'
				  +'<div class="noval alert alert-danger w-100 mb-5" id="norecord">No Record Found</div>'
        +'</div>';
        $('.resources-listing').append(html);
    }

  });
}

function LoadMoreResourcesFilter(page){
  page = page+1;

  var cat_filter = $('.resources_cat_filter').data('resources_cat_filter');
  var search_keyword = $('.resources_cat_filter').data('resources_search_keyword');

  $('.loader').css('display', 'block');
  $.post(location.protocol + "/load_ajax_data", { 'action': 'view_more_resources', 'limit': limit, 'no_of_page': page, 'cat_filter': cat_filter, 'search_keyword': search_keyword }, function(data){
    $('.loader').css('display', 'none'); 

    if(data){
      $('.resources-listing').append(data);
    } else {
      $('.press-loading-example-btn').prop("onclick", null).off("click");
      var html ='<div class="col-12 text-center mt-3" id="norecord">'
				  +'<div class="noval alert alert-danger w-100 mb-5" id="norecord">No Record Found</div>'
        +'</div>';
        $('.resources-listing').append(html);
    }

  });
}

$('#resourcesFiltersDesktop button, #resourcesFiltersMobile button').click(function(){
  $('#resourcesFiltersDesktop button').removeClass('active');
  $('#resourcesFiltersMobile button').removeClass('active');
  $(this).addClass('active');
  
  $('.press-loading-example-btn').on('click', enableclick);
  var resources_cat = $(this).data('cat_filter');

  var page = 0;
  
  $('.resources_cat_filter').data('resources_cat_filter', resources_cat);
  $('.resources-listing').html('');

  LoadMoreResourcesFilter(page);
});

$('.search_data').click(function(){
  $('.press-loading-example-btn').on('click', enableclick);
  var search_keyword = $('#inputSearch').val();

  var page = 0;

  $('.resources_cat_filter').data('resources_search_keyword', search_keyword);
  $('.resources-listing').html('');

  LoadMoreResourcesFilter(page);
});