<?php
/**
 * @file
 * Functions to support theming in the Bartik theme.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Template\Attribute;
use Drupal\taxonomy\Entity\Term;

use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\Core\Url;

use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\block\Entity\Block;
use Drupal\Core\Database\Database;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;

// custom theme functions
include('DataAccess/custom-functions.php');

// theme constants
define('HOME_PAGE_NODE',1);
define('NEWS_LIMIT', 6);
define('EVENTS_LIMIT', 4);
define('FACULTY_LIMIT', 12);
define('CAREERS_LIMIT', 9);
define('RESOURCES_LIMIT', 6);

// block preprocess hook
function hec_preprocess_block(&$variables) {
	$light_logo = theme_get_setting('light_logo');
	$dark_logo = theme_get_setting('dark_logo');

	if($light_logo) {
		$variables['light_logo'] = $light_logo[0];
	}

	if($dark_logo) {
		$variables['dark_logo'] = $dark_logo[0];
	}

	$blockID = $variables['elements']['#id'];
	
    $block = Block::load($blockID);
    $region = $block->getRegion();

	$site_logo = theme_get_setting('logo');
    $variables['site_logo'] = $site_logo['path'];

	$variables['copyright_text'] = theme_get_setting('copyright_text');

	$variables['twitter'] = theme_get_setting('twitter_social_media_icon');
	$variables['youtube'] = theme_get_setting('youtube_social_media_icon');
	$variables['facebook'] = theme_get_setting('facebook_social_media_icon');
	$variables['instagram'] = theme_get_setting('insta_social_media_icon');
	$variables['linkedin'] = theme_get_setting('linkedin_social_media_icon');
	
	$variables['is_front'] = \Drupal::service('path.matcher')->isFrontPage();
	
    if($variables['is_front']) {
	
		if ($blockID == 'programslisting') {
			$variables['programs_list'] = getProgramsList(4);
			$custom_program = theme_get_setting('hec_featured_program');
			foreach($custom_program as $program){
				$programs_data = getNodeDetailsByID($program, 'page');
				$variables['cust_prog_title'] = $programs_data['block_title'];
				$variables['cust_prog_url'] = $programs_data['block_url'];
				if(isset($programs_data['block_image'])){
					$variables['cust_prog_img'] = $programs_data['block_image'];
				}
			}
		}

		if ($blockID == 'newslisting') {
			$variables['news_list'] = getNewsData(3);
		}

		if ($blockID == 'eventslisting') {
			$variables['events_list'] = getEventsList(3);
		}
   
    }

	if ($blockID == 'leadershipdepartmentheads') {
		$variables['leaders_department_heads'] = getLeadersAndDepartmentHeadsList();
	}

	$path_alias = \Drupal::service('path.current')->getPath();	
	$path = \Drupal::service('path.alias_manager')->getPathByAlias($path_alias);
	$node = '';
	
	if(preg_match('/node\/(\d+)/', $path, $matches)) {
		$node = \Drupal\node\Entity\Node::load($matches[1]);
		$type = $node->getType();

		if ($blockID == 'headerbanner') {
			$node_title = $node->get('title')->getValue();
			if($node_title){
				$variables['title'] = $node_title[0]['value'];
			}

			$banner_content = $node->get('field_banner_content')->getValue();
			if($banner_content){
				$variables['banner_content'] = $banner_content[0]['value'];
			} else {
				$variables['banner_content'] = '';
			}

			$page_banner = $node->get('field_banner_image')->getValue();
			$banner_image_data = [];
			$banner_image_val = [];

			if(!empty($page_banner)){
				foreach($page_banner as $banner){
					if (isset($banner)) {
						$file = File::load($banner['target_id']);
						$banner_image_data['url'] = $file->url();
					} else {
						$banner_image_data['url'] = '';
					}
					
					$banner_image_val[] = $banner_image_data;
				}
			
			}

			$page_mob_banner = $node->get('field_mobile_banner_image')->getValue();
			$mob_banner_image_data = [];
			$mob_banner_image_val = [];

			if(!empty($page_mob_banner)){
				foreach($page_mob_banner as $banner){
					if (isset($banner)) {
						$file = File::load($banner['target_id']);
						$mob_banner_image_data['mob_url'] = $file->url();
					} else {
						$mob_banner_image_data['mob_url'] = '';
					}
					
					$mob_banner_image_val[] = $mob_banner_image_data;
				}
			
			}

			$banner_images_data = [];
			$banner_images_val = [];

			if(!empty($banner_image_val)){
				$count = 0;
				foreach($banner_image_val as $banner){
					if (isset($mob_banner_image_val[$count])) {
						$banner_images_data['banner'] = $banner['url'];
						$banner_images_data['mob_banner'] = $mob_banner_image_val[$count]['mob_url'];
					} else {
						$banner_images_data['banner'] = $banner['url'];
						$banner_images_data['mob_banner'] = '';
					}
					
					$banner_images_val[] = $banner_images_data;
					$count++;
				}
			
			}

			$variables['banner_images_val'] = $banner_images_val;
		}

		if($matches[1] == 18){
			$variables['study_steps_title'] = $node->get('field_how_to_study_steps_title')->getValue();

			$field_how_to_study_steps = $node->get('field_how_to_study_steps')->getValue();

			$steps = [];
			$steps_count = 1;
			foreach($field_how_to_study_steps as $study_steps){
				$steps['STEP '.$steps_count] = $study_steps['value'];
				$steps_count++;
			}

			$variables['study_steps'] = $steps;

			$variables['financing_accordion_title'] = $node->get('field_financing_accordion_title')->getValue();

			$financing_accordions = $node->get('field_financing_accordions')->getValue();

			$financing_accordions_data = [];
			$accord_count = 1;

			foreach($financing_accordions as $accordion){
				$accordion_data = getNodeDetailsByID($accordion['target_id'], 'accordions');
				$financing_accordions_data['accord_count'] = $accord_count;
				if($accordion_data['block_title']){
					$financing_accordions_data['block_title'] = $accordion_data['block_title'];
				}
				if($accordion_data['block_content']){
					$financing_accordions_data['block_content'] = $accordion_data['block_content'];
				}
				$finance_accordions[] = $financing_accordions_data;

				$accord_count++;
			}

			$variables['finance_accordions'] = $finance_accordions;

		}
		
		$type = $node->getType();

		if(($matches[1] == 13) || $type == 'page'){
			$field_achievements_slider = $node->get('field_achievements_slider')->getValue();
			$achievements_slider = [];
			$achievements_slides = [];

			foreach($field_achievements_slider as $achievements){
				$file = File::load($achievements['target_id']);
				$achievements_slider['url'] = $file->url();
				$achievements_slider['alt'] = $achievements['alt'];
				$achievements_slider['title'] = $achievements['title'];
				$achievements_slides[] = $achievements_slider;
			}

			$variables['achievements_slider'] = $achievements_slides;

			$field_achievements_slider_background = $node->get('field_achievements_slider_backgr')->getValue();

			$achievements_bg = [];
			if (isset($field_achievements_slider_background[0])) {
				$file = File::load($field_achievements_slider_background[0]['target_id']);
				$achievements_bg['url'] = $file->url();
				$achievements_bg['alt'] = $field_achievements_slider_background[0]['alt'];
			} else {
				$achievements_bg['url'] = '';
				$achievements_bg['alt'] = '';
			}
			
			$variables['achievements_bg'] = $achievements_bg;

			$field_achievements_slider_mob_bg = $node->get('field_achievements_slider_mob_bg')->getValue();

			$achievements_mob_bg = [];
			if (isset($field_achievements_slider_mob_bg[0])) {
				$file = File::load($field_achievements_slider_mob_bg[0]['target_id']);
				$achievements_mob_bg['url'] = $file->url();
				$achievements_mob_bg['alt'] = $field_achievements_slider_mob_bg[0]['alt'];
			} else {
				$achievements_mob_bg['url'] = '';
				$achievements_mob_bg['alt'] = '';
			}
			
			$variables['achievements_mob_bg'] = $achievements_mob_bg;
		}

	}

	$vars['site_url'] = \Drupal::request()->getHost();
}

// page preprocess hook
function hec_preprocess_page(&$vars) { 
	// Remove 'Promoted to front page' nodes
	if ($vars['is_front']) {
		unset($vars['page']['content']['system_main_block']);
	}
	
	drupal_flush_all_caches();	
    $site_logo = theme_get_setting('logo');
	$light_logo = theme_get_setting('light_logo');
	$dark_logo = theme_get_setting('dark_logo');

	$vars['copyright_text'] = theme_get_setting('copyright_text');

	$vars['is_front'] = \Drupal::service('path.matcher')->isFrontPage();
    
    if($vars['is_front']) {
		unset($vars['page']['content']['system_main']['nodes']);

		$node = \Drupal\node\Entity\Node::load(HOME_PAGE_NODE);
		$field_banner_link = $node->get('field_homepage_banner_custom_lin')->getValue();

		if($field_banner_link[0]['value']){
			$vars['banner_link'] = strip_tags($field_banner_link[0]['value']);
		}

		$var_home_banner_video = $node->get('field_homepage_banner_video')->getValue();
		$home_banner_video = [];

		if (isset($var_home_banner_video[0])) {
			$file = File::load($var_home_banner_video[0]['target_id']);
			$home_banner_video['url'] = $file->url();
		} else {
			$home_banner_video['url'] = '';
		}

		$vars['home_banner_video'] = $home_banner_video;

		$page_banner = $node->get('field_banner_image')->getValue();
		$banner_image_data = [];
		$banner_image_val = [];

		if(!empty($page_banner)){
			foreach($page_banner as $banner){
				if (isset($banner)) {
					$file = File::load($banner['target_id']);
					$banner_image_data['url'] = $file->url();
				} else {
					$banner_image_data['url'] = '';
				}
				
				$banner_image_val[] = $banner_image_data;
			}
		
		}

		$page_mob_banner = $node->get('field_mobile_banner_image')->getValue();
		$mob_banner_image_data = [];
		$mob_banner_image_val = [];

		if(!empty($page_mob_banner)){
			foreach($page_mob_banner as $banner){
				if (isset($banner)) {
					$file = File::load($banner['target_id']);
					$mob_banner_image_data['mob_url'] = $file->url();
				} else {
					$mob_banner_image_data['mob_url'] = '';
				}
				
				$mob_banner_image_val[] = $mob_banner_image_data;
			}
		
		}

		$banner_images_data = [];
		$banner_images_val = [];

		if(!empty($banner_image_val)){
			$count = 0;
			foreach($banner_image_val as $banner){
				if (isset($mob_banner_image_val[$count])) {
					$banner_images_data['banner'] = $banner['url'];
					$banner_images_data['mob_banner'] = $mob_banner_image_val[$count]['mob_url'];
				} else {
					$banner_images_data['banner'] = $banner['url'];
					$banner_images_data['mob_banner'] = '';
				}
				
				$banner_images_val[] = $banner_images_data;
				$count++;
			}
		
		}

		$vars['banner_images_val'] = $banner_images_val;
	}

    $vars['site_logo'] = $site_logo['path'];
}

// node preprocess hook
function hec_preprocess_node(&$vars) {  
	$vars['copyright_text'] = theme_get_setting('copyright_text');
	$vars['twitter'] = theme_get_setting('twitter_social_media_icon');
	$vars['youtube'] = theme_get_setting('youtube_social_media_icon');
	$vars['facebook'] = theme_get_setting('facebook_social_media_icon');
	$vars['instagram'] = theme_get_setting('insta_social_media_icon');
	$vars['linkedin'] = theme_get_setting('linkedin_social_media_icon');

    $site_logo = theme_get_setting('logo');
    $vars['site_logo'] = $site_logo['path'];
	
	$path_alias = \Drupal::service('path.current')->getPath();	
	$path = \Drupal::service('path.alias_manager')->getPathByAlias($path_alias);
	
	if(preg_match('/node\/(\d+)/', $path, $matches)) {
		$node = \Drupal\node\Entity\Node::load($matches[1]);
		$type = $node->getType();
		$bundle = $node->getType();

		if (($type == 'page') || ($type == 'programs')) {
			$page_banner = $node->get('field_banner_image')->getValue();
			$banner_image_data = [];
			$banner_image_val = [];

			if(!empty($page_banner)){
				foreach($page_banner as $banner){
					if (isset($banner)) {
						$file = File::load($banner['target_id']);
						$banner_image_data['url'] = $file->url();
					} else {
						$banner_image_data['url'] = '';
					}
					
					$banner_image_val[] = $banner_image_data;
				}
			
			}

			$page_mob_banner = $node->get('field_mobile_banner_image')->getValue();
			$mob_banner_image_data = [];
			$mob_banner_image_val = [];

			if(!empty($page_mob_banner)){
				foreach($page_mob_banner as $banner){
					if (isset($banner)) {
						$file = File::load($banner['target_id']);
						$mob_banner_image_data['mob_url'] = $file->url();
					} else {
						$mob_banner_image_data['mob_url'] = '';
					}
					
					$mob_banner_image_val[] = $mob_banner_image_data;
				}
			
			}

			$banner_images_data = [];
			$banner_images_val = [];

			if(!empty($banner_image_val)){
				$count = 0;
				foreach($banner_image_val as $banner){
					if (isset($mob_banner_image_val[$count])) {
						$banner_images_data['banner'] = $banner['url'];
						$banner_images_data['mob_banner'] = $mob_banner_image_val[$count]['mob_url'];
					} else {
						$banner_images_data['banner'] = $banner['url'];
						$banner_images_data['mob_banner'] = '';
					}
					
					$banner_images_val[] = $banner_images_data;
					$count++;
				}
			
			}

			$vars['banner_images_val'] = $banner_images_val;
		}

		if ($type == 'resources') {
			$resources_accordions = $node->get('field_resources_accordions')->getValue();

			$resources_accordions_data = [];
			$resources_accordions_val = [];
			$accord_count = 1;

			foreach($resources_accordions as $accordion){
				$accordion_data = getNodeDetailsByID($accordion['target_id'], 'resources_accordions');
				$resources_accordions_data['accord_count'] = $accord_count;
				if($accordion_data['block_title']){
					$resources_accordions_data['block_title'] = $accordion_data['block_title'];
				}
				if($accordion_data['block_content']){
					$resources_accordions_data['block_content'] = $accordion_data['block_content'];
				}
				
				$resources_accordions_val[] = $resources_accordions_data;

				$accord_count++;
			}

			$vars['resources_accordions'] = $resources_accordions_val;
		}

		$vars['next_prev_articles'] = getNextPreviousByNodeID($matches[1], $bundle);
		$type = $node->getType();

		if($type == 'article'){	
			$news_date = $node->get('field_date')->getValue();
			$news_dates = [];

			if (isset($news_date[0]['value'])) {
				$vars['news_date'] = date("F Y", strtotime($news_date[0]['value']));
			}
		}

		if($type == 'programs'){	
			$apply_now_link = $node->get('field_ap')->getValue();
			if (isset($apply_now_link[0]['value'])) {
				$vars['apply_now_link'] = $apply_now_link[0]['value'];
			}
			
			$download_brochure_link = $node->get('field_download_brochure_link')->getValue();
			if (isset($download_brochure_link[0]['value'])) {
				$vars['download_brochure_link'] = $download_brochure_link[0]['value'];
			}
			$vars['programs_list'] = getProgramsList(100);
			$testimonials = $node->get('field_testimonials')->getValue();
			$testimonial_arr = [];
			$testimonials_data_arr = [];
			
			foreach($testimonials as $testimonial){
				$testimonial_data = getNodeDetailsByID($testimonial['target_id'], 'testimonials');
				$testimonial_arr['title'] = $testimonial_data['block_title'];
				$testimonial_arr['content'] = $testimonial_data['block_content'];
				$testimonial_arr['qualification'] = $testimonial_data['qualification'];
				$testimonial_arr['image'] = $testimonial_data['block_image'];
				$testimonials_data_arr[] = $testimonial_arr;
			}

			$vars['testimonials'] = $testimonials_data_arr;
		}

		if($matches[1] == 63){
			$menu_links = $node->get('field_sitemap_links_block')->getValue();
			$links_data_arr = [];
			
			foreach($menu_links as $links_data){
				$links_data_arr[] = $links_data;
			}

			$vars['menu_links'] = $links_data_arr;
		}

		if($matches[1] == 24){
			$vars['news_cat'] = getNewsTerms();
			$vars['news_data'] = getNewsData(NEWS_LIMIT);
			$vars['news_dates_filter'] = getNewsDates();
			$vars['total_news_records'] = getAllNewsCount();
			$vars['no_of_page'] = ceil(getAllNewsCount()/NEWS_LIMIT);
			$vars['news_limit'] = NEWS_LIMIT;
			$vars['site_url'] = \Drupal::request()->getHost();
		}
		
		if($matches[1] == 25){
			$vars['programs_list'] = getProgramsList(100);
		}

		if($matches[1] == 26){
			$vars['events_cat'] = getEventsTerms();
			$vars['events_data'] = getEventsList(EVENTS_LIMIT);
			$vars['total_events_records'] = getAllEventsCount();
			$vars['no_of_page'] = ceil(getAllEventsCount()/EVENTS_LIMIT);
			$vars['events_limit'] = EVENTS_LIMIT;
			$vars['site_url'] = \Drupal::request()->getHost();
		}

		if($matches[1] == 34){
			$vars['faculty_data'] = getFacultiesData(FACULTY_LIMIT);
			$vars['total_faculty_records'] = getAllFacultyCount();
			$vars['no_of_page'] = ceil(getAllFacultyCount()/FACULTY_LIMIT);
			$vars['faculty_limit'] = FACULTY_LIMIT;
			$vars['site_url'] = \Drupal::request()->getHost();
		}

		if($matches[1] == 35){
			$vars['career_data'] = getCareersData(CAREERS_LIMIT);
			$vars['total_career_records'] = getCareersCount();
			$vars['no_of_page'] = ceil(getCareersCount()/CAREERS_LIMIT);
			$vars['careers_limit'] = CAREERS_LIMIT;
			$vars['site_url'] = \Drupal::request()->getHost();
		}

		if($matches[1] == 67){
			$vars['resources_cat'] = getResourcesTerms();
			$vars['resources_data'] = getResourcesData(RESOURCES_LIMIT);
			$vars['total_resources_records'] = getResourcesCount();
			$vars['no_of_page'] = ceil(getResourcesCount()/RESOURCES_LIMIT);
			$vars['resources_limit'] = RESOURCES_LIMIT;
			$vars['site_url'] = \Drupal::request()->getHost();
		}
		
	}
}

// page title in head tag
function hec_preprocess_html(&$variables) {
	$light_logo = theme_get_setting('light_logo');
	$dark_logo = theme_get_setting('dark_logo');

	if($light_logo) {
		$variables['light_logo'] = $light_logo[0];
	}

	if($dark_logo) {
		$variables['dark_logo'] = $dark_logo[0];
	}
	
	$path_alias = \Drupal::service('path.current')->getPath();	
	$path = \Drupal::service('path.alias_manager')->getPathByAlias($path_alias);
	
	if(preg_match('/node\/(\d+)/', $path, $matches)) {
		$node = \Drupal\node\Entity\Node::load($matches[1]);
    	$variables['head_title']['title'] = $node->getTitle();
	}

	if (($path_alias == '/user/login') || ($path_alias == '/user/registration')){
		$variables['attributes']['class'][] = 'login-page';
	}

	$statusCode = Drupal::request()->query->get('_exception_statuscode');
    if (isset($statusCode) && ($statusCode == 404)) {
        $variables['attributes']['class'][] = 'page-not-found';
    }

	if ($node = \Drupal::request()->attributes->get('node')) {
		$variables['attributes']['class'][] = 'page-node-' . $node->id();
	}
}